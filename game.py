from random import randint
name = input("Hi! What is your name? ")

for guess in range(5):
    print(f"Guess {int(guess) + 1}: {name} were born in {randint(1,12)}/{randint(1924,2024)}?")
    answer = input("Yes or No? ").lower()
    if answer == "yes":
        print("I knew it!")
        exit()
    elif answer == "no" and int(guess) < 5:
        print("Drat! Lemme try again!")
    elif answer == "exit":
        exit()
    else:
        print("I have other things to do. Good bye.")
